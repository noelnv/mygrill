
#define BLUETOOTH_SPEED 9600
#define SERIAL_SPEED 9600

#include <SoftwareSerial.h>
#include <math.h>

SoftwareSerial bluetooth(10, 11); // RX, TX

int a;
float temperature;
int B=3975;                  //B value of the thermistor
float resistance;

void setup()
{
  Serial.begin(9600);
  bluetooth.begin(BLUETOOTH_SPEED);
}

void loop()
{
  a=analogRead(0);
  resistance=(float)(1023-a)*10000/a; //get the resistance of the sensor;
  temperature=1/(log(resistance/10000)/B+1/298.15)-273.15;//convert to temperature via datasheet ;
  delay(1000);
  Serial.print("Current temperature is ");
  Serial.println(temperature);
  bluetooth.print("Current temperature is ");
  bluetooth.println(temperature);

 }
