#define BLUETOOTH_SPEED 9600
#define SERIAL_SPEED 9600
#include <SoftwareSerial.h>
SoftwareSerial bluetooth(10, 11); // RX, TX

void setup(){
  Serial.begin(SERIAL_SPEED);
  bluetooth.begin(BLUETOOTH_SPEED);
  delay(1000);
}//END SETUP

void loop(){
  waitForResponse();
}


void waitForResponse() {
    delay(1000);
    String cmd = "";
    while (bluetooth.available()) {
      cmd += (char)bluetooth.read();
    }
    if(cmd.length() > 0){
      bluetooth.println(cmd);
      Serial.println(cmd);
    }
}

